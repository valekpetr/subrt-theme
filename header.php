<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

$container = get_theme_mod( 'understrap_container_type' );
?>
  <!DOCTYPE html>
  <html <?php language_attributes(); ?>>

  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,900" />
    <link rel="stylesheet" type="text/css" href="http://cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css" />

    <?php wp_head(); ?>
  </head>

  <body <?php body_class(); ?>>


    <div class="site-wrapper">
      <div class="site-canvas">
        <header class="header">
          <div class="container">
            <div class="row">
              <div class="header__logo">
                <?php 
								the_custom_logo();
              ?>
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                <img class="logo" src="<?php echo get_template_directory_uri();?>/img/logo.svg" alt="Subrt logo" />
              </a>
              </div>
              <div class="header__lang">
                <ul>
                  <?php pll_the_languages(array('show_flags'=>1,));?>
                </ul>
              </div>
            </div>
          </div>
          <nav class="navbar">
            <div class="container">
              <button class="btn btn-hamburger js-mobile-menu-toggle only-mobile-tablet">
                <span>Menu</span>
              </button>
              <?php wp_nav_menu(
							array(
								'theme_location'  => 'primary',
								'container_class' => 'collapse navbar-collapse',
								'menu_class'      => 'navbar-content',
								'fallback_cb'     => '',
                'menu_id'         => 'main-menu',
                'before'          =>  '<a class="btn-close js-mobile-menu-toggle only-mobile-tablet"></a>'
							)
						); ?>

            </div>
          </nav>
        </header>

        </nav>
        <!-- .site-navigation -->


      <!-- .wrapper-navbar end -->
