/*

NOW AS A SUPER EASY TO USE PLUGIN:
https://panels.scotch.io

*/


(function ($) {

  // Toggle Nav on Click
  $('.js-mobile-menu-toggle').on('click',function () {
    $('.site-wrapper').toggleClass('show-nav', 'slow');
  });

  
  $('.js-modal-video').YouTubePopUp();

})(jQuery);