<?php
//
// SLIDER
// -------


function slider_functions( $wp_customize ) {
   
    // Theme layout settings.
    $wp_customize->add_section('slider_options', array(
        'title'       => __( 'Slider', 'subrt' ),
        'description' => __( 'Slider options', 'subrt' ),
        'priority'    => 80,
    ));


    // ITEM 1
    $wp_customize->add_setting('slider_item_one_text', array(
        'default' => '',
        'type' => 'theme_mod',
        'capability' => 'edit_theme_options',
    ));
    $wp_customize->add_control( 'slider_item_one_text', array(
        'type' => 'text',
        'section' => 'slider_options', 
        'label' => __( 'Slider item 1' ),
        'settings' => 'slider_item_one_text',
     ) );

    $wp_customize->add_setting('slider_item_one', array(
        'default' => '',
        'type' => 'theme_mod',
        'capability' => 'edit_theme_options',
    ));
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'slider_item_one', array(
        'section' => 'slider_options',
        'settings' => 'slider_item_one',
    )));




    $wp_customize->add_setting('slider_item_two_text', array(
        'default' => '',
        'type' => 'theme_mod',
        'capability' => 'edit_theme_options',
    ));
    $wp_customize->add_control( 'slider_item_two_text', array(
        'type' => 'text',
        'section' => 'slider_options', 
        'label' => __( 'Slider item 2' ),
        'settings' => 'slider_item_two_text',
     ) );
    $wp_customize->add_setting('slider_item_two', array(
        'default' => '',
        'type' => 'theme_mod',
        'capability' => 'edit_theme_options',
    ));
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'slider_item_two', array(
        'section' => 'slider_options',
        'settings' => 'slider_item_two',
    )));



    $wp_customize->add_setting('slider_item_three_text', array(
        'default' => '',
        'type' => 'theme_mod',
        'capability' => 'edit_theme_options',
    ));
    $wp_customize->add_control( 'slider_item_three_text', array(
        'type' => 'text',
        'section' => 'slider_options', 
        'label' => __( 'Slider item 3' ),
        'settings' => 'slider_item_three_text',
     ) );
    $wp_customize->add_setting('slider_item_three', array(
        'default' => '',
        'type' => 'theme_mod',
        'capability' => 'edit_theme_options',
    ));
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'slider_item_three', array(
        'section' => 'slider_options',
        'settings' => 'slider_item_three',
    )));

 }
add_action( 'customize_register', 'slider_functions' );

