<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package understrap
 */

if ( ! is_active_sidebar( 'left-sidebar' ) ) {
	return;
}

// when both sidebars turned on reduce col size to 3 from 4.
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
?>

	<?php if ( 'both' === $sidebar_pos ) : ?>
	<div class="col-xl-3 widget-area" id="left-sidebar" role="complementary">
		<?php else : ?>
		<div class="col-xl-4 widget-area" id="left-sidebar" role="complementary">
			<?php endif; ?>


			<?php get_template_part( 'global-templates/sidebar-nav', 'none' ); ?>


			<?php dynamic_sidebar( 'left-sidebar' ); ?>
			<!-- #secondary -->
			<div class="footer__content sidebar-contacts">
				<h4 class="footer__title footer__title-red"> <?php _e('Kontakty', 'subrt'); ?></h4>
				<h4> <?php _e('Fakturační adresa:', 'subrt'); ?></h4>
				<p class="icon icon-home">Šubrt Transport
					<br>Koněvova 2660/141
					<br> 130 00 Praha 3
					<br>
				</p>
				<br/>


				<h4><?php _e('Kontakt dispečink:', 'subrt'); ?></h4>
				<p>Ing. René Riedl MBA
					<br>
					<span class="icon icon-phone">
						<a href="tel:420775259444">+420 775 259 444</a>
						<br>
					</span>
					<span class="icon icon-mail">
						<a href="mailto:rene.riedl@subrttransport.cz">rene.riedl@subrttransport.cz</a>
						<br>
					</span>
				</p>
				<br/>
				<h4><?php _e('Kontakt ekonom:', 'subrt'); ?></h4>
				<p>Ing. Bc. Jitka Růžičková
					<br>
					<span class="icon icon-phone">
						<a href="tel:420775791333">+420 775 791 333</a>
						<br>
					</span>
					<span class="icon icon-mail">
						<a href="mailto:jitka.ruzickova@subrttransport.cz">jitka.ruzickova@subrttransport.cz</a>
						<br>
					</span>
				</p>

			</div>
		</div>