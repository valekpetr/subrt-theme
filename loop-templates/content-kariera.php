<?php
/**
 * Partial template for content in page.php
 *
 * @package understrap
 */

?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<header class="page-header">


	<?php the_title( '<h2 class="page-title">', '</h2>' ); ?>

	
	</header><!-- .entry-header -->
	<?php
// 	$attr  = array(
// 			'class'	=> "page-image",
// 		);
// 		echo get_the_post_thumbnail( $post->ID, 'large', $attr);
// ?>

	<div class="entry-content" style="text-transform:none">

		<?php the_content(); ?>

		<?php
		wp_link_pages( array(
			'before' => '<div class="page-links">' . __( 'Pages:', 'subrt' ),
			'after'  => '</div>',
		) );
		?>

	</div><!-- .entry-content -->

	<footer class="entry-footer">

		<?php //edit_post_link( __( 'Upravit', 'subrt' ), '<span class="edit-link">', '</span>' ); ?>

	</footer><!-- .entry-footer -->

</article><!-- #post-## -->
