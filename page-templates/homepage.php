<?php
/**
 * Template Name: Homepage
 *
 * This template can be used to override the default template and sidebar setup
 *
 * @package understrap
 */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );

?>


<div class="wrapper" id="page-wrapper">

<?php get_template_part( 'global-templates/hero', 'none' ); ?>

<?php get_template_part( 'global-templates/boxes', 'none' ); ?>

<?php the_content(); ?>

</div><!-- Wrapper end -->

<?php get_footer(); ?>
