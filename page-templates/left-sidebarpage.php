<?php
/**
 * Template Name: Left Sidebar Layout
 *
 * This template can be used to override the default template and sidebar setup
 *
 * @package understrap
 */

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper" id="page-wrapper">
<header class="page-bg" style="background-image: url('<?php echo get_the_post_thumbnail_url();?>');">
	<div class="container">
		<div class="row">
			<div class="col">
			<h1 class="page-main-title">	<?php the_title();?></h1>
				<div class="breadcumb">
							<ul>
								<li class="home-link">
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php _e('Domovská stránka', 'subrt'); ?></a>
								</li>
								<li class="separator">/</li>
								<li class="active">
								<?php the_title();?>
								</li>
							</ul>
						</div>
			</div>
		</div>

	</div>
</header>

<?php 
$detect = new Mobile_Detect;
if( !$detect->isMobile() || !$detect->isTablet() ){
	get_template_part( 'global-templates/boxes', 'none' );
}
?>


	<div class="<?php echo esc_attr( $container ); ?>" id="content">

		<div class="row">

			<?php get_sidebar( 'left' ); ?>

			<div
				class="<?php if ( is_active_sidebar( 'left-sidebar' ) ) : ?>col-xl-8<?php else : ?>col-md-12<?php endif; ?> content-area"
				id="primary">

				<main class="site-main" id="main" role="main">

					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'loop-templates/content', 'page' ); ?>

						<?php
						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;
						?>

					<?php endwhile; // end of the loop. ?>

				</main><!-- #main -->

			</div><!-- #primary -->

		</div><!-- .row -->

	</div><!-- Container end -->
	<?php 
if( $detect->isMobile() || $detect->isTablet() ){	
	get_template_part( 'global-templates/boxes', 'none' );
}
?>
</div><!-- Wrapper end -->

<?php get_footer(); ?>
