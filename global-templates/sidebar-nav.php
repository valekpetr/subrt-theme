<?php if (!is_page( 10 )) : ?>
<a href="/o-nas">
<div class="services__box row sidebar-box">
	<div class="services__box-icon sidebar-box-icon col-3">
		<img src="<?php echo get_template_directory_uri();?>/img/sidebar-onas.svg" />
	</div>
	<div class="services__box-content sidebar-box-content col-9">
		<h4 class="services__box-title sidebar-box-title"><?php _e('Proč s námi?', 'subrt'); ?></h4>
	</div>
</div>
</a>
<a href="/kariera">
<div class="services__box row sidebar-box">
<div class="services__box-icon sidebar-box-icon col-3">
	<img src="<?php echo get_template_directory_uri();?>/img/sidebar-kariera.svg" />
</div>
<div class="services__box-content sidebar-box-content col-9">
	<h4 class="services__box-title sidebar-box-title"><?php _e('Kariéra', 'subrt'); ?></h4>
</div>
</div>
</a>
<a href="/reference">
<div class="services__box row sidebar-box">
<div class="services__box-icon sidebar-box-icon col-3">
	<img src="<?php echo get_template_directory_uri();?>/img/sidebar-reference.svg" />
</div>
<div class="services__box-content sidebar-box-content col-9">
	<h4 class="services__box-title sidebar-box-title"><?php _e('Reference', 'subrt'); ?></h4>
</div>
</div>
</a>
<a href="/kontakty">
<div class="services__box row sidebar-box">
<div class="services__box-icon sidebar-box-icon col-3">
	<img src="<?php echo get_template_directory_uri();?>/img/sidebar-kontakty.svg" />
</div>
<div class="services__box-content sidebar-box-content col-9">
	<h4 class="services__box-title sidebar-box-title"><?php _e('Kontakty', 'subrt'); ?></h4>
</div>
</div>
</a>
<!-- <?php //else: ?>

<div class="services__box row sidebar-box">

		<div class="services__box-icon sidebar-box-icon col-3">
			<img src="<?php //echo get_template_directory_uri();?>/img/services-doprava.svg" />
		</div>
		<div class="services__box-content sidebar-box-content col-9">
			<h4 class="services__box-title sidebar-box-title">
				<?php //_e('Vozový park', 'subrt'); ?>
			</h4>
		</div>

</div>
<div class="services__box row sidebar-box">
<div class="services__box-icon sidebar-box-icon col-3">
	<img src="<?php //echo get_template_directory_uri();?>/img/services-zasilky.svg" />
</div>
<div class="services__box-content sidebar-box-content col-9">
	<h4 class="services__box-title sidebar-box-title">
		<?php //_e('Sledování zásilek', 'subrt'); ?>
	</h4>
</div>
</div>
<div class="services__box row sidebar-box">
<div class="services__box-icon sidebar-box-icon col-3">
	<img src="<?php //echo get_template_directory_uri();?>/img/services-pojisteni.svg" />
</div>
<div class="services__box-content sidebar-box-content col-9">
	<h4 class="services__box-title sidebar-box-title">
		<?php //_e('Pojištění zásilek', 'subrt'); ?>
	</h4>
</div>
</div>

<?php endif; ?> -->
