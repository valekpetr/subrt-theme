<?php
/**
 * Boxes.
 *
 * @package understrap
 */

?>
<section class="section-boxes">
        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-md-6 col-lg-3 first">
            <?php 
              switch (getLang()) {
                case 'cs':
                $pageUrl = 298;
                break;
                
                case 'en':
                $pageUrl = 302;
                break;
                
                case 'de':
                $pageUrl = 344;
                break;
              }
            ?>  
              <a href="<?php echo esc_url( get_page_link( $pageUrl ) ); ?>">
                <div class="box box-doprava">
                  <div class="box-bg"></div>
                  <h2 class="box-title"><?php _e('Mezinárodní doprava', 'subrt');?></h2>
                  <div class="box-arrow"></div>
                </div>
              </a>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-3">
            <?php 
              switch (getLang()) {
                case 'cs':
                $pageUrl = 38;
                break;
                
                case 'en':
                $pageUrl = 347;
                break;
                
                case 'de':
                $pageUrl = 349;
                break;
              }
            ?>  
              <a href="<?php echo esc_url( get_page_link( $pageUrl ) ); ?>">
                  <div class="box box-spedice">
                    <div class="box-bg"></div>
                    <h2 class="box-title"><?php _e('Spedice', 'subrt');?></h2>
                    <div class="box-arrow"></div>
                  </div>
                </a>
              </div>
              <div class="col-xs-12 col-md-6 col-lg-3">
            <?php 
              switch (getLang()) {
                case 'cs':
                $pageUrl = 39;
                break;
                
                case 'en':
                $pageUrl = 321;
                break;
                
                case 'de':
                $pageUrl = 351;
                break;
              }
            ?>  
              <a href="<?php echo esc_url( get_page_link( $pageUrl ) ); ?>">
                <div class="box box-sklad">
                  <div class="box-bg"></div>
                  <h2 class="box-title"><?php _e('Skladování', 'subrt');?></h2>
                  <div class="box-arrow"></div>
                </div>
              </a>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-3">
            <a href="https://youtu.be/BrbH6BjGuaY" class="js-modal-video">
                <div class="box box-video">
                  <div class="box-bg"></div>
                  <h2 class="box-title"><?php _e('Video', 'subrt');?></h2>
                  <div class="box-arrow"></div>
                </div>
              </a>
            </div>
          </div>
        </div>
      </section>