<section class="our-clients">
        <div class="container">
          <div class="row">
            <h3 class="our-clients__title"><?php _e('Naši klienti', 'subrt');?></h3>
          </div>
          <div class="row">
            <div class="our-clients__item col-sm">
              <img src="<?php echo get_template_directory_uri(); ?>/img/client-db-schenker.png" alt="DB Schenker" />
            </div>
            <div class="our-clients__item col-sm">
              <img src="<?php echo get_template_directory_uri(); ?>/img/client-dachser.png" alt="Dachser" />
            </div>
            <div class="our-clients__item col-sm">
              <img src="<?php echo get_template_directory_uri(); ?>/img/client-dhl.png" alt="DHL" />
            </div>
            <div class="our-clients__item col-sm">
              <img src="<?php echo get_template_directory_uri(); ?>/img/client-dsv.png" alt="DSV" />
            </div>
            <div class="our-clients__item col-sm">
              <img src="<?php echo get_template_directory_uri(); ?>/img/client-ewals.png" alt="Ewals" />
            </div>
          </div>
        </div>
      </section>