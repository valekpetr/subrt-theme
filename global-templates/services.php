<section class="section-services">
  <div class="container">
    <div class="row">
      <h2 class="services__title"><?php _e('Naše služby', 'subrt');?></h2>
      <div class="col-md">
        <!-- <a href="/mezinarodni-doprava"> -->
          <div class="services__box row">
            <div class="services__box-icon col-3">
              <img src="<?php echo get_template_directory_uri(); ?>/img/services-doprava.svg" />
            </div>

            <div class="services__box-content col-9">
              <h3 class="services__box-title"><?php _e('MEZINÁRODNÍ DOPRAVA', 'subrt');?></h3>
              <!-- <div class="services__box-perex">Potřebujete rychle poradit, napište nám. Quistium solut quuntiis platem eum, omnitas archici.</div> -->
            </div>
          </div>
        <!-- </a> -->

        <!-- <a href="/sledovani-zasilek"> -->
          <div class="services__box row">
            <div class="services__box-icon col-3">
              <img src="<?php echo get_template_directory_uri(); ?>/img/services-zasilky.svg" />
            </div>

            <div class="services__box-content col-9">
              <h3 class="services__box-title"><?php _e('SLEDOVÁNÍ ZÁSILEK', 'subrt');?></h3>
              <!-- <div class="services__box-perex">Potřebujete rychle poradit, napište nám. Quistium solut quuntiis platem eum, omnitas archici.</div> -->
            </div>
          </div>
        <!-- </a> -->
        
        <!-- <a href="/spedice"> -->
          <div class="services__box row">
            <div class="services__box-icon col-3">
              <img class="smaller" src="<?php echo get_template_directory_uri(); ?>/img/services-spedice.svg" />
            </div>

            <div class="services__box-content col-9">
              <h3 class="services__box-title"><?php _e('SPEDICE', 'subrt');?></h3>
              <!-- <div class="services__box-perex">Potřebujete rychle poradit, napište nám. Quistium solut quuntiis platem eum, omnitas archici.</div> -->
            </div>
          </div>
        <!-- </a> -->
        
      </div>
      <div class="col-md">
        <!-- <a href="/vozovy-park"> -->
          <div class="services__box row">
            <div class="services__box-icon col-3">
              <img src="<?php echo get_template_directory_uri(); ?>/img/services-doprava.svg" />
            </div>
            <div class="services__box-content col-9">
              <h3 class="services__box-title"><?php _e('VOZOVÝ PARK', 'subrt'); ?></h3>
              <!-- <div class="services__box-perex">Potřebujete rychle poradit, napište nám. Quistium solut quuntiis platem eum, omnitas archici.</div> -->
            </div>
          </div>
        <!-- </a> -->
        <!-- <a href="/pojisteni-zasilek"> -->
          <div class="services__box row">
            <div class="services__box-icon col-3">
              <img class="smaller" src="<?php echo get_template_directory_uri(); ?>/img/services-pojisteni.svg" />

            </div>

            <div class="services__box-content col-9">
              <h3 class="services__box-title"><?php _e('POJIŠTĚNÍ ZÁSILEK', 'subrt'); ?></h3>
              <!-- <div class="services__box-perex">Potřebujete rychle poradit, napište nám. Quistium solut quuntiis platem eum, omnitas archici.</div> -->
            </div>
          </div>
        <!-- </a> -->


        <!-- <a href="/skladovani"> -->
          <div class="services__box row">
            <div class="services__box-icon col-3">
              <img src="<?php echo get_template_directory_uri(); ?>/img/services-sklad.svg" />
            </div>

            <div class="services__box-content col-9">
                  <h3 class="services__box-title"><?php _e('SKLADOVÁNÍ', 'subrt'); ?></h3>
                  <!-- <div class="services__box-perex">Potřebujete rychle poradit, napište nám. Quistium solut quuntiis platem eum, omnitas archici.</div> -->
            </div>
          </div>
        <!-- </a> -->
        
      </div>

    </div>
    <div class="row">
      <div class="col" style="text-align: center;">
      <?php 
              switch (getLang()) {
                case 'cs':
                $pageUrl = 298;
                break;
                
                case 'en':
                $pageUrl = 302;
                break;
                
                case 'de':
                $pageUrl = 344;
                break;
              }
            ?>  
              <a class="section-services__link" href="<?php echo esc_url( get_page_link( $pageUrl ) ); ?>">
                <?php _e('Více informací', 'subrt'); ?>
                <span class="box-arrow"></span>
            </a>
      </div>
    </div>
</section>