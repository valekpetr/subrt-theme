<section class="contact-us">
        <div class="container">

      <?php 
      switch (getLang()) {
        case 'cs':
        echo do_shortcode('[cf7-form cf7key="kontaktni-formular-1"]');
        break;
        
        case 'en':
        echo do_shortcode('[contact-form-7 id="329" title="Contact form"]');
        break;
        
        case 'de':
        echo do_shortcode('[contact-form-7 id="330" title="Kontaktformular"]');
        break;
      }
       ?>     

      </section>
