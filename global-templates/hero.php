<?php
/**
 * Hero.
 *
 * @package understrap
 */

 function slider_background($bg_url) {
	if ( 0 < count( strlen( ( $background_image_url = get_theme_mod( $bg_url ) ) ) ) ) { 
		
		return $bg_url = $background_image_url;
	 } 
 }
 function slider_text($sliderItem_text) {

	return get_theme_mod($sliderItem_text);
	
 }
?>
	<div class="slider">
		<ul>
			<li class="slider-item" style="background-image: url( <?php echo get_template_directory_uri(); ?>/img/slider1.jpg ); ">
				<h1 class="slider-item__content">
					<span><?php _e('Rychlé a kvalitní služby za příznivé ceny', 'subrt')?></span>
				</h1>
			</li>
			<li class="slider-item" style="background-image: url( <?php echo get_template_directory_uri(); ?>/img/slider2.jpg ); ">
					<h1 class="slider-item__content">
						<span><?php _e('Specialista na silniční nákladní dopravu po Evropě', 'subrt')?></span>
					</h1>
				</li>
				<li class="slider-item" style="background-image: url( <?php echo get_template_directory_uri(); ?>/img/slider3.jpg ); ">
						<h1 class="slider-item__content">
							<span><?php _e('Hlavní specializací jsou denní přepravy do Velké Británie a Irska', 'subrt')?></span>
				</h1>
			</li>
		</ul>
	</div>