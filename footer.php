<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

$the_theme = wp_get_theme();
$container = get_theme_mod( 'understrap_container_type' );
?>

<?php get_sidebar( 'footerfull' ); ?>
<?php if (!is_front_page()) : ?>
	<?php get_template_part( 'global-templates/contact-us', 'none' ); ?>
<?php endif; ?>

<footer class="footer">
<div class="container">
	<div class="row">
		<div class="col-md-5">
			<h3 class="footer__title footer__title-red"><?php _e('Kontakty', 'subrt'); ?></h3>
		</div>
		<div class="col-md-7">
			<h3 class="footer__title footer__title-black only-tablet-desktop"><?php _e('Informace', 'subrt'); ?></h3>
		</div>
	</div>
	<div class="footer__content">
		<div class="row">
			<div class="col-md">
				<h4><?php _e('Fakturační adresa', 'subrt'); ?>:</h4>
				<p class="icon icon-home">Šubrt Transport
					<br>Koněvova 2660/141
					<br> 130 00 Praha 3
					<br>
				</p>
				<br/>
			</div>
			<div class="col-md">
				<h4><?php _e('Kontakt dispečink', 'subrt'); ?>:</h4>
				<p>Ing. René Riedl MBA
					<br>
					<span class="icon icon-phone"><a href="tel:420775259444">+420 775 259 444</a>
						<br>
					</span>
					<span class="icon icon-mail"><a href="mailto:rene.riedl@subrttransport.cz">rene.riedl@subrttransport.cz</a>
						<br>
					</span>
				</p>
				<br/>
				<h4><?php _e('Kontakt ekonom', 'subrt'); ?>:</h4>
				<p>Ing. Bc. Jitka Růžičková
					<br>
					<span class="icon icon-phone"><a href="tel:420775791333">+420 775 791 333</a>
						<br>
					</span>
					<span class="icon icon-mail"><a href="mailto:jitka.ruzickova@subrttransport.cz">jitka.ruzickova@subrttransport.cz</a>
						<br>
					</span>
				</p>
			</div>
			<div class="col-md-2">
				<h3 class="footer__title footer__title-black only-mobile"><?php _e('Informace', 'subrt'); ?></h3>
				<?php wp_nav_menu(
							array(
								'theme_location'  => 'footer',
								'container_class' => 'collapse navbar-collapse',
								'menu_class'      => 'footer-links',
								'fallback_cb'     => '',
                'menu_id'         => 'main-menu'
							)
						); ?>
				
			</div>
			<div class="col-md">
			<?php wp_nav_menu(
							array(
								'theme_location'  => 'footer2',
								'container_class' => 'collapse navbar-collapse',
								'menu_class'      => 'footer-links',
								'fallback_cb'     => '',
                'menu_id'         => 'main-menu'
							)
						); ?>
				
			</div>
			<div class="col-md footer__lang">
				<ul>
				<?php pll_the_languages(array('show_flags'=>1,));?>
				</ul>
			</div>
		</div>
	</div>
	<div class="row copyright">
		<div class="col-12">
			<p>
			  <?php _e('Všechna práva vyhrazena.', 'subrt'); ?>
				Copyright © 2009–<?php echo date( 'Y' ); ?> 
				<br> <?php _e('Vytvořeno firmou', 'subrt'); ?> <a href="http://lepor.cz" rel="nofollow">Lepor</a>
			</p>
		</div>
	</div>
</div>
</footer>

</div><!-- canvas end -->


</div><!-- wrapper menu end -->





<?php wp_footer(); ?>

</body>

</html>

